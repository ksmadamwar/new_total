from django.apps import AppConfig


class TotalappConfig(AppConfig):
    name = 'totalapp'
