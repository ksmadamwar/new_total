Django==2.0.8
django-pyodbc-azure<2.1
pyodbc==4.0.26
requests==2.9.1
django-cors-headers==3.0.2
pymongo==3.9.0
pymongo-document-modeling==0.9.2.dev1

